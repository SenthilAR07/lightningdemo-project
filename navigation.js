import { Utils } from "@lightningjs/sdk"
import MenuImages from '../body/menuImages'

export default class Navigation extends lng.Component {
    static _template() {
        return {
            NavBar: {
                w: 400, h: 1080, color: 0xfff74a28, rect: true,
                SearchImage: { w: 40, h: 40, x: 100, y: 50, src: Utils.asset('images/search.png'), },
                List: {type: NavMenu}
            },
            BodyImage: {type: MenuImages},
        }
    } 

    _init() {
        this.tag('List').items = [{icon:'images/discover.png', text:'Discover'}, {icon:'images/filter.png', text:'Categories'},
        {icon:'images/aptoide.png', text:'My Apps'}, {icon:'images/community.png', text:'Community'},
        {icon:'images/settings.png', text:'Settings'}]

        this._setState('List')
    }
    _handleRight(){
        this._setState('BodyImage')
    }
    static _states(){
        return[
            class List extends this {
                _getFocused(){
                    return this.tag('List')
                }
            },
            class BodyImage extends this {
                _getFocused(){
                    return this.tag('BodyImage')
                }
            }
        ]
    }
}

class NavMenu extends lng.Component {
    _init(){
        this.index = 0;
    }
    set items(items){
        this.children = items.map((item,index) => {
            return {
                type: NavMenuItem,
                y: index * 90,
                item
            }
        }
        )}
    _getFocused() {
        return this.children[this.index]
    }
    _handleUp(){
        if(this.index > 0){
            this.index--
        }
    }
    _handleDown(){
        if(this.index < this.children.length - 1){
            this.index++
        }
    }
}

class NavMenuItem extends lng.Component {
    static _template() {
        return{
            Label: {
                x: 150, y: 200,color: 0xffffffff, 
              },
              Icon: {
                x: 100, y: 200,
              }
        }
    }

    _init() {
        this.patch({ Label: { text: { text: this.item.text , fontSize: 32, fontFace: 'Trebuchet MS', }}}),
        this.patch({ Icon: {w:40, h: 40, src: Utils.asset(this.item.icon)}})
    }
    _focus(){
        this.patch({ smooth: { alpha: 1, scale: 1.1}})
    }
    _unfocus() {
        this.patch({ smooth: { alpha: 0.8, scale: 1}})
    }
}