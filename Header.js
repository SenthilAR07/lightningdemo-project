import { Utils } from "@lightningjs/sdk";

export default class Header extends lng.Component {
    static _template() {
        return {
            Header: {
                w: 400, h: 100, x: 1450, y: 50,
                AppImage: { w: 60, h: 60, x: 10, src: Utils.asset('images/app.png'), },
                Text: { w: 300, h: 80, x: 90, text: { text: "Aptoide TV", fontFace: 'Regular', fontSize: 50, textColor: 0xffffffff, } }
            },
        }
    };
}