import { Utils } from "@lightningjs/sdk"

export default class MenuImages extends lng.Component {
    static _template(){
        return{
            FirstRow: {
                w: 1300, h: 400, y: 400, x: 500,
                ListOne: {type: MenuImagesIter}
              },
              SecondRow: {
                w: 1300,h: 400,y: 680, x: 500,
                ListTwo: {type: MenuImagesIter}
              },
            //   ThirdRow: {
            //     w: 1300,h: 400,y: 700, x: 500,
            //     ListThree: {type: MenuImagesIter}
            //   },
        }
    }
    _init() {
        this.tag('ListOne').items = [{icon:'images/youtube.png',text:'Youtube'}, {icon:'images/primevideo.png', text: 'Amaxon Prime'}, {icon:'images/netflix.png', text:'Netflix'}],
        this.tag('ListTwo').items = [{icon:'images/plex.jpeg',text:'Plex'}, {icon:'images/teamviewer.png',text:'TeamViewer'}, {icon:'images/perfect.png',text:'Perfect'}],
        // this.tag('ListThree').items = [{icon:'images/hbo.jpg'}, {icon:'images/startsports.jpg'}, {icon:'images/tv_img.png'}]

        this._setState('ListOne')
      }

    _handleUp() {
        this._setState('ListOne')
    }
    _handleDown() {
        this._setState('ListTwo')
    }
    
    static _states(){
        return[
            class ListOne extends this{
                _getFocused() {
                    return this.tag('ListOne')
                }
            },
            class ListTwo extends this{
                _getFocused() {
                    return this.tag('ListTwo')
                }
            }
            ]
    }
}

class MenuImagesIter extends lng.Component {
    _init(){
        this.index = 0;
    }

    set items(items){
        this.children = items.map((item,index) => {
            return {
                type: MenuImageIterList,
                x: index * 450,
                item
            }
        })
    }
    _getFocused() {
        return this.children[this.index]
    }
    _handleLeft() {
        if(this.index > 0) {
            this.index--
        }else {
            console.log('else')
            this.fireAncestors('$stateChange')
        }
    }
    _handleRight() {
        if(this.index < this.children.length - 1) {
          this.index++
        }
    }
}

class MenuImageIterList extends lng.Component {
    static _template(){
        return{
            Icon: {x: 100},
            Label: {x: 100 , y: -50,color: 0xffffffff, }
        }
    }

    _init(){
        this.patch({ Icon: {w:400, h: 200, src: Utils.asset(this.item.icon)}})
    }
    _focus() {
        this.patch({ Label: { text: { text: this.item.text , fontSize: 32, fontFace: 'Trebuchet MS', }}}),
        this.patch({ smooth: { alpha: 1, scale: 1.1},w: 500, h: 250})
    }
    _unfocus() {
        this.patch({ Label: { text: { text: ''}}}),
        this.patch({ smooth: { alpha: 0.8, scale: 1}})
    }
}